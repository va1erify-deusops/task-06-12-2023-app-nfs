# Репозиторий с приложением django-filesharing

#### Этот репозиторий содержит исходники приложения django-filesharing, Dockerfile, Helm-charts и CI для build/upload в registry

## Структура репозитория (основные файлы)
```
.
|-- ci-files                       CI файлы для build/upload docker-image и helm-charts 
|-- django-filesharing             Директория с приложением
|   |-- Dockerfile                 Dockerfile для build docker image
|   |-- filesharing
|   |   |-- settings.py            Конфигурационные настройки
|   |-- public                     Директория, указанная в settings.py для загрузки файлов
|   |   `-- static
|   |-- requirements.txt           Список зависимостей Python
`-- helm-charts                    Директория с helm чартами
    `-- nfs-app
        |-- Chart.yaml             Файл метаданных Helm чарта, содержащий информацию о версии, имени, описании и других связанных параметрах чарта
        |-- templates              Директория, содержащая шаблоны Kubernetes манифестов, необходимых для развертывания приложения
        |   |-- deployment.yml     Файл манифеста для развертывания приложения в виде Deployment
        |   |-- ingress.yml        Файл манифеста для настройки входящего трафика в приложение с использованием Ingress
        |   |-- namespace.yml      Файл манифеста для создания отдельного Namespace
        |   |-- pvc.yaml           Файл манифеста для создания Persistent Volume Claim (PVC) в Kubernetes, который используется для запроса хранилища NFS для приложения
        |   `-- service.yml        Файл манифеста для создания Service, предоставляющего доступ к приложению внутрикластера.
        `-- values.yaml            Файл со значениями по умолчанию для Helm чарта. Здесь определяются параметры, которые могут быть переопределены при установке чарта с Helm.
```
## Развертывание вручную
### 1. Клонировать репозиторий:


    git clone https://gitlab.com/va1erify-deusops/task-06-12-2023-app-nfs.git


### 2. Перейти в директорию:


    cd django-filesharing

### 3. Установить зависимости:


    pip3 install -r requirements.txt


### 4. Запустить приложение:


    python3 ./manage.py runserver 0.0.0.0:8000


### 5. Открыть браузер и перейдите по адресу http://localhost:8000


## Развертывание через docker image


### 1. Установка Docker

Убедитесь, что Docker установлен на вашем хосте. Если Docker не установлен, следуйте инструкциям для вашей операционной системы на [официальном сайте Docker](https://docs.docker.com/get-docker/).

### 2. Получение Docker образа из Registry

Используйте следующую команду, чтобы загрузить Docker образ из Container Registry:


    docker pull registry.gitlab.com/va1erify-deusops/task-06-12-2023-app-nfs/django-filesharing-test:<версия>
    docker pull registry.gitlab.com/va1erify-deusops/task-06-12-2023-app-nfs/django-filesharing-prod:<версия>


### 3. Запуск Docker контейнера

Создайте и запустите контейнер на основе загруженного Docker образа с помощью следующей команды:

    docker run -d -p <внешний_порт>:<внутренний_порт(8000)> --name <имя_контейнера> registry.gitlab.com/va1erify-deusops/task-06-12-2023-app-nfs/django-filesharing:<версия>

### 4. Проверка работоспособности приложения

    Откройте браузер и перейдите на http://localhost:<внешний_порт> (замените <внешний_порт> на порт, указанный в шаге 3), чтобы убедиться, что ваше приложение успешно развернуто и работает.

## Развертывание через helm

### 1. Добавление репозитория Helm


    helm repo add --username $USERNAME --password $ACCESS_TOKEN $HELM_REPO_NAME https://gitlab.com/api/v4/projects/55241996/packages/helm/stable


### 2. Обновление Helm репозитория

    helm repo update


### 3. Установка Helm Chart

    helm install $HELM_CHART_NAME $HELM_REPO_NAME/$PACKAGE_NAME --version $APP_VERSION

- **APP_VERSION**: Определяет версию приложения для установки (например, 1.0.0).
- **HELM_REPO_NAME**: Название Helm репозитория (какое будет "name" при "helm repo list")
- **PACKAGE_NAME**: Название пакета в Package Registry
- **HELM_CHART_NAME**: Под каким названием chart будет задеплоен в k8s.


## Как происходит build/upload

### 1. Стадии

Репозиторий использует одну стадию:

`module-pipelines`: Включает два пайплайна для сборки и загрузки Docker-образа и Helm-чарта.

```
                  .build-upload-docker-image.yml
.gitlab-ci.yml ->
                  .build-upload.helm-chart.yml
```

### 2. Пайплайны

#### Пайплайн `build-upload-docker-image`

Этот пайплайн собирает и загружает Docker-образ. 

#### Пайплайн `build-upload-helm-chart`

Этот пайплайн собирает и загружает Helm-чарт.


## Prod/Test окружение 

### Необходимо изменить конфигурацию в файле "/django-filesharing/filesharing/settings.py"

- Для тестовой среды:
```
DEBUG = True

ALLOWED_HOSTS = ['*']
```

- Для промышленной среды:

```
DEBUG = False

ALLOWED_HOSTS = ['Список хостов для доступа']
```

## Структура Helm chart
```
---
# Source: nfs-app/templates/namespace.yml
apiVersion: v1
kind: Namespace
metadata:
  name: nfs-app
```

```
---
# Source: nfs-app/templates/pvc.yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: nfs-dir-pvc
  namespace: nfs-app
spec:
  resources:
    requests:
      storage: 100Mi
  accessModes:
    - ReadWriteMany
  storageClassName: "nfs-client" # название по умолчанию
```

```
---
# Source: nfs-app/templates/service.yml
apiVersion: v1
kind: Service
metadata:
  name: nfs-app
  namespace: nfs-app
  labels:
    app: nfs-app
spec:
  type: ClusterIP
  ports:
    - port: 8000
      protocol: TCP
      targetPort: 8000
  selector:
    app: nfs-app

```

```
---
# Source: nfs-app/templates/deployment.yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nfs-app
  namespace: nfs-app
  labels:
    app: nfs-app
spec:
  revisionHistoryLimit: 15
  replicas: 1
  selector:
    matchLabels:
      app: nfs-app
  template:
    metadata:
      labels:
        app: nfs-app
    spec:
      containers:
        - name: nfs-app
          image: registry.gitlab.com/va1erify-deusops/task-06-12-2023-app-nfs/django-filesharing-prod:1.0.0
          imagePullPolicy: Always
          ports:
            - name: nfs-app
              containerPort: 8000
          volumeMounts:
            - name: docker-socket
              mountPath: /tmp/docker.sock
            - name: nfs-dir
              mountPath: /build/public/static
      volumes:
        - name: docker-socket
          hostPath:
            path: /var/run/docker.sock
        - name: nfs-dir
          persistentVolumeClaim:
            claimName: nfs-dir-pvc
```

```
---
# Source: nfs-app/templates/ingress.yml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nfs-app
  namespace: nfs-app
  annotations:
    kubernetes.io/ingress.class: nginx
    cert-manager.io/cluster-issuer: "letsencrypt-prod"
    nginx.ingress.kubernetes.io/proxy-read-timeout: "6000"
    nginx.ingress.kubernetes.io/proxy-send-timeout: "6000"
spec:
  tls:
  - hosts:
    - nfs-app.valery-rayumov.ru
    secretName: app-tls # создает IngressController
  rules:
  - host: nfs-app.valery-rayumov.ru
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: nfs-app
            port:
              number: 8000
```